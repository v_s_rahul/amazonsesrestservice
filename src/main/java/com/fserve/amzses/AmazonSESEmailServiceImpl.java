package com.fserve.amzses;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;

@Service
public class AmazonSESEmailServiceImpl implements ISendEmailService {

	private static final Logger log = LoggerFactory.getLogger(AmazonSESEmailServiceImpl.class);

	private AmazonSimpleEmailServiceClient client;

	//@PostConstruct
	public void init(AWSCredentials credentials) {
		try {
			client = new AmazonSimpleEmailServiceClient(credentials);
			client.setRegion(Region.getRegion(Regions.US_EAST_1));
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
					+ "Please make sure that your credentials file is at the correct "
					+ "location (~/.aws/credentials), and is in valid format.", e);
		}
	}

	@Override
	public void sendEmail(String subjectStr, String bodyStr, String toAddress) throws IOException {
		SendEmailRequest request = new SendEmailRequest().withDestination(new Destination().withToAddresses(toAddress))
				.withMessage(new Message()
						.withBody(new Body().withText(new Content().withCharset("UTF-8").withData(bodyStr)))
						.withSubject(new Content().withCharset("UTF-8").withData(subjectStr)))
				.withSource("admin@ephaseglobal.com");
		try {
			log.info("Attempting to send an email through Amazon SES by using the AWS SDK for Java...");
			client.sendEmail(request);
			log.info("Email sent!");
		} catch (Exception ex) {
			log.info("The email was not sent.");
			log.error("Error message: " + ex.getMessage(),ex);
		}
	}

	@Override
	public void sendEmail(String subjectStr, String bodyStr, String htmlStr, String toAddress) throws IOException {
		SendEmailRequest request = new SendEmailRequest().withDestination(new Destination().withToAddresses(toAddress))
				.withMessage(new Message()
						.withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(htmlStr))
								.withText(new Content().withCharset("UTF-8").withData(bodyStr)))
						.withSubject(new Content().withCharset("UTF-8").withData(subjectStr)))
				.withSource("admin@ephaseglobal.com");
		try {
			log.info("Attempting to send an email through Amazon SES by using the AWS SDK for Java...");
			client.sendEmail(request);
			log.info("Email sent!");
		} catch (Exception ex) {
			log.info("The email was not sent.");
			log.error("Error message: " + ex.getMessage(),ex);
		}
	}

	@Override
	public void sendEmail(String subjectStr, String bodyStr, String htmlStr, String toAddress, File[] files)
			throws AddressException, MessagingException, IOException {
		String DefaultCharSet = MimeUtility.getDefaultJavaCharset();

		Session session = Session.getDefaultInstance(new Properties());

		// Create a new MimeMessage object.
		MimeMessage message = new MimeMessage(session);

		// Add subject, from and to lines.
		message.setSubject(subjectStr, "UTF-8");
		message.setFrom(new InternetAddress("admin@ephaseglobal.com"));
		message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(toAddress));

		// Create a multipart/alternative child container.
		MimeMultipart msg_body = new MimeMultipart("alternative");

		// Create a wrapper for the HTML and text parts.
		MimeBodyPart wrap = new MimeBodyPart();

		// Define the text part.
		MimeBodyPart textPart = new MimeBodyPart();
		// Encode the text content and set the character encoding. This step is
		// necessary if you're sending a message with characters outside the
		// ASCII range.
		textPart.setContent(MimeUtility.encodeText(bodyStr, DefaultCharSet, "B"), "text/plain; charset=UTF-8");
		textPart.setHeader("Content-Transfer-Encoding", "base64");

		// Define the HTML part.
		MimeBodyPart htmlPart = new MimeBodyPart(); // Encode the HTML content
													// and set the character
													// encoding.
		htmlPart.setContent(MimeUtility.encodeText(htmlStr, DefaultCharSet, "B"), "text/html; charset=UTF-8");
		htmlPart.setHeader("Content-Transfer-Encoding", "base64");

		// Add the text and HTML parts to the child container.
		msg_body.addBodyPart(textPart);
		// msg_body.addBodyPart(htmlPart);

		// Add the child container to the wrapper object.
		wrap.setContent(msg_body);

		// Create a multipart/mixed parent container.
		MimeMultipart msg = new MimeMultipart("mixed");

		// Add the parent container to the message.
		message.setContent(msg);

		// Add the multipart/alternative part to the message.
		msg.addBodyPart(wrap);

		MimeBodyPart att;
		DataSource fds;

		for (File f : files) {
			// Define the attachment
			att = new MimeBodyPart();
			fds = new FileDataSource(f);
			att.setDataHandler(new DataHandler(fds));
			att.setFileName(fds.getName());

			// Add the attachment to the message.
			msg.addBodyPart(att);
		}
		// Try to send the email.
		try {
			log.info("Attempting to send an email through Amazon SES " + "using the AWS SDK for Java...");

			// Print the raw email content to log
			log.info(message.toString());

			// Send the email.
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			message.writeTo(outputStream);
			RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));

			SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
			// .withConfigurationSetName(CONFIGURATION_SET);

			client.sendRawEmail(rawEmailRequest);
			log.info("Email sent!");
			// Display an error if something goes wrong.
		} catch (Exception ex) {
			log.info("Email Failed");
			log.error("Error message: " + ex.getMessage(),ex);
		}

	}

}
