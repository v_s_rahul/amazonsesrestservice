package com.fserve.amzses;

import java.io.File;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

/*
 *  Amazon SES is being leveraged for sending email. 
 */

public interface ISendEmailService {
	
	/**
	 * simple email like hello world.
	 * 
	 * @param subjectStr
	 * @param bodyStr
	 * @param toAddress
	 * @throws IOException
	 */
	void sendEmail(String subjectStr, String bodyStr, String toAddress) throws IOException;
	
	/**
	 * email with rich content like html
	 * 
	 * @param subjectStr
	 * @param bodyStr
	 * @param htmlStr
	 * @param toAddress
	 * @throws IOException
	 */
	void sendEmail(String subjectStr, String bodyStr, String htmlStr, String toAddress) throws IOException;

	/**
	 * email with body and attachment.
	 * 
	 * @param subjectStr
	 * @param bodyStr
	 * @param htmlStr
	 * @param toAddress
	 * @param file
	 * @throws IOException
	 * @throws MessagingException 
	 * @throws AddressException 
	 */
	void sendEmail(String subjectStr, String bodyStr, String htmlStr, String toAddress, File[] files) throws IOException, AddressException, MessagingException;

}
