package com.fserve;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmzonSesRestClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmzonSesRestClientApplication.class, args);
	}
}
