package com.fserve.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.fserve.amzses.AmazonSESEmailServiceImpl;

@RestController
public class AmzSESRestController {
	
	static final String BODY = "This email was sent through Amazon SES by using the AWS SDK for Java.";
	static final String SUBJECT = "Amazon SES test (AWS SDK for Java)";
	static final String HTMLBODY = "<h1>Amazon SES test (AWS SDK for Java)</h1>"
		      + "<p>This email was sent with <a href='https://aws.amazon.com/ses/'>"
		      + "Amazon SES</a> using the <a href='https://aws.amazon.com/sdk-for-java/'>" 
		      + "AWS SDK for Java</a>";
	
	@Autowired
	private AmazonSESEmailServiceImpl client;
	
	@PostConstruct
	private void init(){
		 AWSCredentials credentials = new BasicAWSCredentials("AKIAI6BDR5N2ZXSR732Q",
					"P02pjbDmPnYyH/vESlc53FhC6BuNk3YfrnWffkBz");
		 client.init(credentials);
	        
	}
	
	@RequestMapping(value = "/fileupload", headers=("content-type=multipart/*"), method = RequestMethod.POST)
	 public boolean upload(@RequestParam("file") MultipartFile inputFile) {
     
		try{
		File f = new File(inputFile.getOriginalFilename());
        f.createNewFile(); 
        FileOutputStream fos = new FileOutputStream(f); 
        fos.write(inputFile.getBytes());
        fos.close(); 
       // inputFile.transferTo(f);
        File[] fArr = new File[1];
        fArr[0] = f;
        
       
        client.sendEmail(SUBJECT, BODY, HTMLBODY, "v_s_rahul@rediffmail.com", fArr);
        
		}catch(Exception e){
			return false;
		}
        
		return true;
		
	}

}
